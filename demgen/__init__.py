# -*- coding: utf-8 -*-
"""
Created on Thu Oct 24 09:58:07 2019

@author: aandre
"""
from math import sqrt

import numpy as np


def z(r):
    return -r/sqrt(2.) + 40.


def dist(x, y):
    return sqrt(x*x + y*y)


def i2l(i, resolution):
    return i*resolution + resolution/2.


def square(grid, radius):
    rows, cols = grid.shape
    left = np.zeros((rows, radius))

    for j in range(radius):
        left[..., j] = grid[..., 0]

    return np.concatenate([left, grid], 1)


def genCone(resolution, radius, slope):
    width, height = radius + 1, 2*radius + 1
    interceipt = slope*((radius + .5)*sqrt(2.)*resolution)
    gridCenter = (0, radius)
    # zMax = -slope*i2l(0, resolution) + interceipt

    center = (i2l(gridCenter[0], resolution), i2l(gridCenter[1], resolution))

    x = np.linspace(0.0 + resolution/2., width*resolution - resolution/2., width)
    y = np.linspace(0.0 + resolution/2., height*resolution - resolution/2., height)
    X, Y = np.meshgrid(x, y)

    D = np.sqrt((X - center[0])**2 + (Y - center[1])**2)

    Z = -slope*D + interceipt

    grid = square(Z, radius)

    return grid
