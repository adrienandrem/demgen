#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 25 16:03:50 2019

@author: aandre
"""
import argparse

from math import exp, log


def gaussian(x, m, s, l, epsilon):
    return x - (((l - m)*(l - m))/2.0)*exp(-2.0*x) + 0.5 - log(epsilon/s)


def gaussian_derivative(x, m, l):
    return 1.0 + ((l - m)*(l - m))*exp(-2.0*x)


def gaussian_adjust(mean, slope, distance, epsilon):
    u = 0.0
    y = gaussian(u, mean, slope, distance, epsilon)

    while(abs(y) > 0.00000001):
        u = u - y/gaussian_derivative(u, mean, distance)
        y = gaussian(u, mean, slope, distance, epsilon)

    c = exp(u)

    return slope*exp(0.5)*c, c


if __name__ == "__main__":
    # execute only if run as a script

    PARSER = argparse.ArgumentParser(prog='gaussian', usage='%(prog)s [options]')
    PARSER.add_argument('mean', type=float, help='Mean')
    PARSER.add_argument('slope', type=float, help='Max slope')
    PARSER.add_argument('distance', type=float, help='Distance where 0')
    PARSER.add_argument('epsilon', type=float, help='Epsilon (almost 0 value)')
    ARGS = PARSER.parse_args()

    MEAN = ARGS.mean
    SLOPE = ARGS.mean
    DISTANCE = ARGS.mean
    EPSILON = ARGS.epsilon

    a, c = gaussian_adjust(MEAN, SLOPE, DISTANCE, EPSILON)

    print('a, c')
    print(a, c)
