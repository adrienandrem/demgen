# -*- coding: utf-8 -*-
"""
Created on Thu May 30 12:12:10 2019

@author: aandre
"""

from pytest import approx, mark

from math import sqrt

from demgen import z, dist, i2l, genCone


def test_z():
    assert z(0) == approx(40)
    assert z(40*sqrt(2.)) == approx(0)


def test_dist():
    assert dist(0, 0) == approx(0)
    assert dist(40, 0) == approx(40)
    assert dist(0, 40) == approx(40)
    assert dist(-40, 0) == approx(40)


def test_i2l():
    resolution = 5.

    assert i2l(0, resolution) == approx(2.5)
    assert i2l(1, resolution) == approx(7.5)
    assert i2l(2, resolution) == approx(12.5)
    assert i2l(3, resolution) == approx(17.5)


@mark.skip(reason="Not implemented")
def test_cone_0():
    resolution = 5.
    radius = 4
    slope = 10./100.

    elevation = genCone(resolution, radius, slope)

    assert elevation[0][4] == approx(2.25)

    assert elevation[0][0] == approx(0.25)
    assert elevation[4][4] == approx(0.25)
    assert elevation[8][0] == approx(0.25)


@mark.skip(reason="Not implemented")
def test_gaussian_0():
    pass
