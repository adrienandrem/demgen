#include <iostream>
#include <math.h>

struct parameters {
    double arg;
    double a;
    double c;
};

double  g(double &x, double &m, double &s, double &l, double &epsilon) {
    return x - (((l - m)*(l - m))/2.0)*exp(-2.0*x) + 0.5 - log(epsilon/s);
}

double  derivative_g(double &x, double &m, double &l) {
    return 1.0 + ((l - m)*(l - m))*exp(-2.0*x);
}

parameters gauss_adj(double &m, double &s, double &l, double &epsilon) {
   double u, y;
   u = 0.0 ;
   y = g(u, m, s, l, epsilon);
   while(abs(y) > 0.00000001) {
      u = u - y / derivative_g(u, m, l);
      y = g(u, m, s, l, epsilon);
   }
   struct parameters solution;
   solution.arg = u;
   solution.c = exp(u);
   solution.a = s*exp(0.5)*solution.c;
   
   return solution;
}

int main() {
   double l, m, s, epsilon;
   struct parameters coeff;
   
   std::cout << "Set the point of maximum of the gaussian (tne mean): m = ";
   std::cin >> m;
   std::cout << "Set the maximum slope: s = ";
   std::cin >> s;
   std::cout << "Set the point at which the gaussian does not exceed a certain treshold: l = ";
   std::cin >> l;
   std::cout << "Set the treshold of the gaussian at point l: epsilon = ";
   std::cin >> epsilon;
   
   coeff = gauss_adj(m, s, l, epsilon);
   
   std::cout << "The gaussian function should have the formula: \n";
   std::cout << "y = " << coeff.a << "*exp((x - " << m << ")^2/(2*" << coeff.c << "^2)) \n";
   std::cout << "The parameter u = " << coeff.arg << "\n";
}
