#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Thu May 30 11:50:30 2019

@author: aandre
"""
import os
import sys

import logging

import argparse

import numpy as np

import rasterio
from rasterio.transform import from_origin
from rasterio.crs import CRS

from demgen import i2l, genCone


logging.basicConfig(level=os.environ.get('LOGLEVEL', 'INFO'))


CRS_EPSG = 2972

SHAPES = ['cone', 'gaussian']


def genGaussian(resolution, radius, slope):
    width, height = radius + 1, 2*radius + 1
    gridCenter = (0, radius)

    center = (i2l(gridCenter[0], resolution), i2l(gridCenter[1], resolution))

    x = np.linspace(0.0 + resolution/2., width*resolution - resolution/2., width)
    y = np.linspace(0.0 + resolution/2., height*resolution - resolution/2., height)
    X, Y = np.meshgrid(x, y)

    D = np.sqrt((X - center[0])**2 + (Y - center[1])**2)

    a, b, c = 3., 0., 1.5

    Z = a*np.exp(-(D - b)**2/(2*c**2))
    logging.debug('Z:\n%s', Z)

    return Z


def saveToFile(elevation, resolution, filename):

    transform = from_origin(0., elevation.shape[0]*resolution,
                            resolution, resolution)

    dataset = rasterio.open(
        filename, 'w', driver='AAIGrid',
        height=elevation.shape[0], width=elevation.shape[1],
        count=1,
        dtype=elevation.dtype,
        crs=CRS.from_epsg(CRS_EPSG), transform=transform)

    dataset.write(elevation, 1)
    dataset.close()


def main(shape, resolution, radius, slope, filename):

    if shape == 'cone':
        logging.info('Generating cone shaped DEM')
        elevation = genCone(resolution, radius, slope/100.)
    else:
        logging.info('Generating gaussian shaped DEM')
        elevation = genGaussian(resolution, radius, slope/100.)

    logging.debug('Elevation.\n%s', elevation)

    logging.info('Saving to file %s', filename)
    grid = np.round(elevation, 2)
    saveToFile(grid, resolution, filename)
    np.savetxt('%s.txt' % filename, grid, delimiter=' ', fmt='%.2f')


if __name__ == "__main__":

    OUTDIR = '/data/out'

    PARSER = argparse.ArgumentParser()
    PARSER.add_argument('shape', help='Shape type: cone, gaussian', default='cone')
    PARSER.add_argument('resolution', help='Raster resolution', type=float, default=5.)
    PARSER.add_argument('radius', help='Shape radius', type=int, default=4)
    PARSER.add_argument('slope', help='Maximum slope percentage', type=float, default=10.)
    PARSER.add_argument('filename')
    ARGS = PARSER.parse_args()

    SHAPE = ARGS.shape
    RESOLUTION = ARGS.resolution
    RADIUS = ARGS.radius
    SLOPE = ARGS.slope
    FILENAME = ARGS.filename

    # Check input data
    if not SHAPE in SHAPES:
        logging.error('Incorrect shape. Must be one of %s', SHAPES)
        sys.exit(1)

    main(SHAPE, RESOLUTION, RADIUS, SLOPE, FILENAME)
