# demgen

DEM generator.

Generates an elevation grid from grid size, slope shape and maximum value.

## Usage

To generate a 5m resolution 7x7 grid of 22% slope cone shaped relief:

    mkdem cone 5 3 22 cone-5-3-22.asc
